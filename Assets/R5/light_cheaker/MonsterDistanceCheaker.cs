﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDistanceCheaker : MonoBehaviour
{


    [SerializeField]
    private AudioSource _HeartbeatSource;
    [SerializeField]
    public float monsterMinDistance;


    public Transform m_MonsterArr;


    public Transform[] m_MonsterPosition;
    public int m_Childs;

    public Transform m_player;
    public float _HeartbeatSpeed=1f;


  //  float m_voL = 0;

    void Start()
    {
        m_player = this.transform;

    

        m_Childs = m_MonsterArr.childCount;
        int allMonsterCount=0;
        for (int i = 0; i < m_Childs; i++)
        {
            allMonsterCount += m_MonsterArr.GetChild(i).transform.childCount;
        }
        m_MonsterPosition= new Transform[allMonsterCount];


        int m_monsterItem = 0;

        for (int i = 0; i < m_MonsterArr.childCount; i++)
        {


            for (int p = 0; p < m_MonsterArr.GetChild(i).childCount; p++)
            {
                


                m_MonsterPosition[m_monsterItem] = m_MonsterArr.GetChild(i).GetChild(p).transform;

                m_monsterItem++;
            }



        }

    }

    // Update is called once per frame
    void Update()
    {

        monsterMinDistance= monsterCheak();



        float m_voL = 0;
        if (monsterMinDistance < 12)
        {
            float mathTest;
            mathTest = monsterMinDistance / 12;
            _HeartbeatSpeed = 2f - (mathTest* mathTest)*(2f-0.7f);
        }
      

        else
        {
            _HeartbeatSpeed = 0.7f;
        }

        m_voL = _HeartbeatSpeed*0.5f;


        _HeartbeatSource.pitch = Mathf.Lerp(_HeartbeatSource.pitch, _HeartbeatSpeed,0.5f);

        _HeartbeatSource.volume = Mathf.Lerp(_HeartbeatSource.volume, m_voL, 0.5f);




    }




    float  monsterCheak()
    {

        float minDistance = 100;

        for (int i = 0; i < m_MonsterPosition.Length; i++)
        {

            float monster_Distance = Vector3.Distance(m_MonsterPosition[i].position, m_player.position);

            if(minDistance > monster_Distance)

            {
                minDistance = Vector3.Distance(m_MonsterPosition[i].position, m_player.position);
            }
        }
        return minDistance;
    }











}
