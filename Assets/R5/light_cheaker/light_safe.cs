﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class light_safe : MonoBehaviour
{

   public Transform m_zeroSpace;
   public Transform m_Light;
    [Range(0, 1)]
    public float safe_Distance = 0.05f;

    Ray m_raytest;
    RaycastHit hitpoint;
    void Start()
    {
      
    }


   


    // Update is called once per frame
    void Update()
    {
        m_raytest = new Ray(transform.position, transform.forward);


        if (    Physics.Raycast(m_raytest, out hitpoint, Vector3.Distance(transform.position, m_zeroSpace.position),~(1<<15 ) ) )
  
        {
            m_Light.position = Vector3.Lerp(m_Light.position,  hitpoint.point+((transform.position- hitpoint.point).normalized* safe_Distance) , 0.5f);

        }
        else 

         {
            m_Light.position = Vector3.Lerp(m_Light.position, m_zeroSpace.position + ((transform.position - m_zeroSpace.position).normalized * safe_Distance), 0.5f);
        
         }


    }




    private void OnDrawGizmos()   //畫射線對照
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(m_raytest);
    }






}
