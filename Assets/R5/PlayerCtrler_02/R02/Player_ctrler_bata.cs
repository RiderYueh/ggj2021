﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_ctrler_bata : MonoBehaviour
{
    // SetplayerState m_SetState_scp;




    Vector3 hitpointN;



    [Range(0, 5)]
    public float checkHight = 2f;

    [Range(0, 5)]
    public float fallPower = 1;


    Vector3 fallMove;




    float smmothLerp = 0;

   

   public bool m_IsGrounded;

    Ray GroundedCheckerRay;

    RaycastHit hitpoint;







    [HeaderAttribute("---Player Setting---")]///////////////////////////////////////////////////////////////

    public Animator m_Animator;      // 動畫控制器
    public Transform m_PlayerModel;

  //  public Transform m_Camera;       // 第一人稱控制攝影機
   // public Transform m_CameraTarget;       // 第一人稱控制攝影機

    [SpaceAttribute]//空行

    [HeaderAttribute("---Player State---")]////////////////////////////////////////////////////////////////////

    [SerializeField]
    private PlayerState m_playerState = PlayerState.idel;
    [HeaderAttribute("-Right")]
    [SerializeField]
    private R_handState m_R_handState = R_handState.Relax;
    [SerializeField]
    private R_ArmState m_R_armState = R_ArmState.Relax;
    [HeaderAttribute("-Left")]
    [SerializeField]
    private L_ArmState m_L_armState = L_ArmState.Relax;
    [SerializeField]
    private L_handState m_L_handState = L_handState.Relax;

    [HeaderAttribute("----------Skeleton-body----------")] //黑字////////////////////////////////////////////////
    [SpaceAttribute]

    public Transform Neck;

    public Transform Head;

    public Transform bodyBone;

    public Transform spine_1;

    public Transform spine_0;

    [HeaderAttribute("-shoulder-bone")] //黑字
    [SerializeField]
    public Transform R_shoulder;

    public Transform L_shoulder;
  

    [HeaderAttribute("----------IK-Target-body----------")] //黑字////////////////////////////////////////////////////
    [SpaceAttribute]

    public Transform Controllroot;   // 全身控制器的Root

    public Transform bodyForwarTdtarget;

    public Transform Camera_Axis;   //攝影機及主要物件的旋轉軸
    public Transform BodyIKTarget;

    [HeaderAttribute("-shoulder_target")] //黑字

    public Transform R_shoulder_Target;
    [Range(0, 1)]
    public float R_shoulderWeight = 1;

    public Transform R_armRelaxTarget;
    [Range(0, 1)]
    public float R_armRelaxWeight = 1;

    [HeaderAttribute("---------------")] //黑字
    public Transform L_shoulder_Target;
    [Range(0, 1)]
    public float L_shoulderWeight = 1;

    public Transform L_armRelaxTarget;
    [Range(0, 1)]
    public float L_armRelaxWeight = 1;






    [HeaderAttribute("--------Controller_Data--------")] //黑字////////////////////////////////////////////////////////
    [SpaceAttribute]
    [Range(0, 10)]
    public float AirMoveSpeed = 3;
  
    [Range(1, 100)]
    public float ChangeLayerSpeed = 8;

    [Range(30f, 60f)]
    public float TurnCheckAngle = 30f;   //上下半身夾角確認
    [Range(0f, 720f)]
    public float TurnSpeed_X = 180; //攝影機水平旋轉速動
    [Range(0f, 720f)]
    public float TurnSpeed_Y = 180; //攝影機垂直旋轉速動
    [HeaderAttribute("-TurnUpAngle")] //黑字
    [Range(0f, -90f)]
    public float TurnAngle_Min = -70;//y最小角度
    [Range(0f, -60f)]
    public float RunAngle_Min = -15;//y最大角度
    [HeaderAttribute("-TurnDown")] //黑字
    [Range(0f, 90f)]
    public float TurnAngle_Max = 90;//y最大角度
    [Range(0f, 55)]
    public float RunAngle_Max = 55;//y最大角度
    [HeaderAttribute("-----LeftHand&bodyDamp-----")] //黑字
    [Range(0.1f, 150)]
    public float DampWeight = 45;

    // public bool m_bool = false;

    // 私有變量//////////////////////////////////////////////////////////////////////////////////////////////

   
    private float m_camera_X = 0;
    private float m_camera_Y = 0;
    private Quaternion CM_Rotation = new Quaternion();
    private Vector3 bodyForward = new Vector3();
    private Vector3 bodyRight = new Vector3();
    private Vector3 followForward = new Vector3();
    [SerializeField]
    private float Tdt;
    private float TurnAngleUp;
    private float TurnAngleDown;
    private Rigidbody m_rigidbody;
    private CharacterController m_Ctrler;

  float m_MoveAxis_V ;
   float m_MoveAxis_H ;



    private Quaternion L_shoulder_ROT;
    private Quaternion R_shoulder_ROT;
    private Quaternion Ctrler_ROT;



    public enum PlayerState  //主狀態
    {
        idel,
        Run,
        Walk,
        InAir,

    }
    public enum R_handState  //右手手掌狀態
    {
        Relax,
        GetArms,

    }
    public enum L_handState   //左手手掌狀態
    {
        Relax,
        GetFlashlight,
    }
    public enum R_ArmState  //右手手臂狀態
    {
        Relax,
        Throw,
        ShowArms,
        GetArms,


    }
    public enum L_ArmState  //左手手臂狀態
    {
        Relax,
        Flashlight,
    }






    
    void Start()
    {
        m_rigidbody = this.GetComponent<Rigidbody>();
         Cursor.lockState = CursorLockMode.Locked;
        m_Ctrler = this.GetComponent<CharacterController>();


        TurnAngleDown = TurnAngle_Max;  ///
        TurnAngleUp = TurnAngle_Min;    ///

        Vector3 angles = Camera_Axis.transform.eulerAngles;  //攝影機角度
        m_camera_X = angles.y;
        m_camera_Y = angles.x;

        m_L_handState = L_handState.GetFlashlight;


        ResetForward();

    }



  


    private void Update()
    {
       
        Tdt = Time.deltaTime;
       
        
        SetGravity();
        slideSet();
  

        ResetForward();
        DebugOverTurn(transform.forward, m_PlayerModel.forward);


        m_MoveAxis_H = Input.GetAxis("Horizontal");
        m_MoveAxis_V = Input.GetAxis("Vertical");
   
        m_Animator.SetFloat("move_h", m_MoveAxis_H);
        m_Animator.SetFloat("move_v", m_MoveAxis_V);


        Player_Move();


        CheakCameraAngle(); //確認是否在跑步狀態 調整攝影機

        CameraRotSet();  ///

        Turn_chack(transform.forward, m_PlayerModel.forward, TurnCheckAngle);  ///
       

        Check_PlayerState();


        if (Input.GetKey(KeyCode.Mouse1))
        {
          //  m_R_armState = R_ArmState.ShowArms;
            m_Animator.SetBool("ShowArms", true);
        }
        else
        {
           // m_R_armState = R_ArmState.Relax;
            m_Animator.SetBool("ShowArms", false);
        }




        //if (Input.GetKey(KeyCode.Mouse1))

        //{

        //    m_L_armState = L_ArmState.Flashlight;


        //}
        //else
        //{
        //    m_L_armState = L_ArmState.Relax;
        //}
        



          


        Set_L_handState();
        Check_R_armState();

        //Check_R_handState();
        //Check_L_handState();
      //  Check_L_armState();


        SetR_ArmAniWeight();
        SetL_ArmAniWeight();


    }







    void LateUpdate()
    {
        
        Ctrler_ROT = Controllroot.rotation;
        Controllroot.rotation = m_PlayerModel.rotation;
        Camera_Axis.rotation = bodyBone.rotation;

        SetRelaxShoulder();///test

        Controllroot.rotation = Ctrler_ROT;
        Camera_Axis.rotation = CM_Rotation;
        SetShoulderTargetWeight();

      // spine_0.forward = Vector3.Lerp(transform.forward, BodyIKTarget.forward, 0.25f);
        spine_1.forward = Vector3.Lerp(transform.forward, BodyIKTarget.forward, 0.3f);


        bodyBone.rotation =  BodyIKTarget.rotation;

        Neck.rotation = Quaternion.Euler(m_camera_Y * 0.75F, m_camera_X, 0.0f);
        Head.rotation = CM_Rotation;
       

        SetShoulderForTarget();
     

        BodyIKTarget.position = new Vector3(bodyBone.position.x,
          Mathf.Lerp(BodyIKTarget.position.y, bodyBone.position.y, DampWeight * Tdt),
         bodyBone.position.z);

        

    }
    private void OnDrawGizmos()   //畫射線對照
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + m_PlayerModel.forward);

        //Gizmos.color = Color.cyan;
        //Gizmos.DrawLine(BodyIKTarget.position, bodyIk_target.position);

        Gizmos.color = Color.blue;

        Gizmos.DrawLine(transform.position, transform.forward + transform.position);

     
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, followForward + transform.position);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.right + transform.position);
        Gizmos.color = Color.black;
        Gizmos.DrawLine(R_shoulder.position, R_shoulder.forward + R_shoulder.position);
    }


 



    void ResetForward() //重置正面方向
    {
       
       // Controllroot.position = this.transform.position; //重置旋轉軸

        bodyForward = bodyForwarTdtarget.position - Controllroot.position;
        bodyForward.y = 0;
        bodyForward.Normalize();

        bodyRight = Vector3.Cross(Vector3.up, bodyForward);
        bodyRight.Normalize();
    }



    void Set_L_handState()  //設定左手狀態  再有拿到手電筒的情況 可以按下  Alpha1  舉起手
    {
        if (m_L_armState == L_ArmState.Relax)
        {

            if (Input.GetKeyDown(KeyCode.Alpha1)&& m_L_handState== L_handState.GetFlashlight)
            {
                m_L_armState = L_ArmState.Flashlight;
                
            }
        }

        else if (m_L_armState == L_ArmState.Flashlight)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                m_L_armState = L_ArmState.Relax;
            }
        }

    }



    void Player_Move()//設定狀態
    {

        if (m_IsGrounded)//m_IsGrounded)

        {
          

            if (m_MoveAxis_H != 0 || m_MoveAxis_V != 0)
            {

                if (Input.GetKey(KeyCode.LeftShift) && m_MoveAxis_V >= 0.5f)

                { m_playerState = PlayerState.Run; }


                else { m_playerState = PlayerState.Walk; }


            }

            else m_playerState = PlayerState.idel;
        }
        else
        {
           m_Ctrler.Move( (((bodyForward*m_MoveAxis_V )+ (bodyRight*m_MoveAxis_H)).normalized) *AirMoveSpeed*Tdt);

            m_playerState = PlayerState.InAir;
        
        }



    } 


   

    private void Reset_Turn_ani()  //重置 m_Animator的Trigger("turn_L"&"turn_R");
    {
        m_Animator.ResetTrigger("turn_L");
        m_Animator.ResetTrigger("turn_R");
    }

    void Turn_chack(Vector3 body, Vector3 feet, float turn_angle)
    {

        m_Animator.applyRootMotion = true;
      
        Reset_Turn_ani();

     

        if (m_playerState == PlayerState.idel)
        {

            if (Vector3.Angle(body, feet) >= turn_angle)

            {
                followForward = body;

                m_Animator.applyRootMotion = false;

                if (Vector3.Cross(body, feet).y <= 0)  //判斷左右轉
                {
                    m_Animator.SetTrigger("turn_R");
                    if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("TURN_L"))
                        smmothLerp = 0;
                }
                else
                {
                    m_Animator.SetTrigger("turn_L");

                    if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("TURN_R"))
                        smmothLerp = 0;
                }
            }

            else m_PlayerModel.Rotate(Vector3.up, Input.GetAxis("Mouse X") * -TurnSpeed_X * Tdt);



            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("TURN_L") ||
            m_Animator.GetCurrentAnimatorStateInfo(0).IsName("TURN_R"))

            {
                m_Animator.applyRootMotion = false;

                if (Vector3.Angle(body, feet) >= 5)
                {

                    followForward = body;

                   m_PlayerModel.rotation = Quaternion.Slerp(m_PlayerModel.rotation, Quaternion.LookRotation(body), smmothLerp * Tdt);
                    if (smmothLerp < 10)
                        smmothLerp++;
                }
            }

            else
            {
   
                smmothLerp = 0;
            }
        }
        
       
    }   //旋轉確認

   
    void CameraRotSet()  //主要旋轉控制
    {

       
        m_camera_X += Input.GetAxis("Mouse X") * TurnSpeed_X * Tdt;
        m_camera_Y -= Input.GetAxis("Mouse Y") * TurnSpeed_Y * Tdt;
        m_camera_Y = ClampAngle(m_camera_Y, TurnAngleUp, TurnAngleDown);

        
        this.transform.rotation = Quaternion.Euler(0.0f, m_camera_X, 0.0f);

        CM_Rotation = Quaternion.Euler(m_camera_Y, m_camera_X, 0.0f);

        BodyIKTarget.rotation = Quaternion.Euler(m_camera_Y * 0.5F, m_camera_X, 0.0f);


    }



    void SetRelaxShoulder()
    {

        R_armRelaxTarget.rotation = R_shoulder.rotation;

        L_armRelaxTarget.rotation = L_shoulder.rotation;

        R_shoulder_Target.rotation = R_shoulder.rotation;

        L_shoulder_Target.rotation = L_shoulder.rotation;


    }
    

    void SetShoulderForTarget()
    {
        L_shoulder.rotation = L_shoulder_ROT;
        R_shoulder.rotation = R_shoulder_ROT;
    }

    void SetShoulderTargetWeight()
    {
        R_shoulder_ROT = Quaternion.Lerp(R_armRelaxTarget.rotation, R_shoulder_Target.rotation, R_armRelaxWeight);
        L_shoulder_ROT = Quaternion.Lerp(L_armRelaxTarget.rotation, L_shoulder_Target.rotation, L_armRelaxWeight);
       
    }











    private float ClampAngle(float angle, float min, float max)  // 旋轉角度限制在360度內
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }   
    void CheakCameraAngle()   //確認是否在跑步狀態 調整攝影機
    {

        if (m_playerState==PlayerState.Run)
        { 
            TurnAngleDown = Mathf.Lerp(TurnAngleDown, RunAngle_Max, 1 * Tdt * 5F);
            TurnAngleUp = Mathf.Lerp(TurnAngleUp, RunAngle_Min, 1 * Tdt * 5F);
        }

        else
        {
            TurnAngleDown = Mathf.Lerp(TurnAngleDown, TurnAngle_Max, 1 * Tdt * 5F);
            TurnAngleUp = Mathf.Lerp(TurnAngleUp, TurnAngle_Min, 1 * Tdt * 5F);
        }
    }

    //private void ResetTarget()
    //{


    //    //右邊
    //    //右肩
    //    if (R_shoulder_Target && R_shoulder)
    //        R_shoulder_Target.position = R_shoulder.position;



    //    //右腕
    //    if (R_arm_Target && R_arm)
    //        R_arm_Target.position = R_arm.position;
    //    //右前臂
    //    if (R_foreArmTarget && R_foreArm)
    //        R_foreArmTarget.position = R_foreArm.position;
    //    //右手
    //    if (R_hand_Target && R_hand)
    //        R_hand_Target.position = R_hand.position;



    //    //左邊
    //    //左肩
    //    if (L_shoulder_Target && L_shoulder)
    //        L_shoulder_Target.position = L_shoulder.position;
    //    //左腕
    //    if (L_arm_Target && L_arm)
    //        L_arm_Target.position = L_arm.position;
    //    //左前臂
    //    if (L_foreArmTarget && L_foreArm)
    //        L_foreArmTarget.position = L_foreArm.position;
    //    //左手
    //    if (L_hand_Target && L_hand)
    //        L_hand_Target.position = L_hand.position;





    //}
    //private void ReSetTargetRotate()
    //{


    //    //右邊
    //    //右肩
    //    if (R_shoulder_Target && R_shoulder)
    //        R_shoulder_Target.rotation = R_shoulder.rotation;
    //    //右腕
    //    if (R_arm_Target && R_arm)
    //        R_arm_Target.rotation = R_arm.rotation;
    //    //右前臂
    //    if (R_foreArmTarget && R_foreArm)
    //        R_foreArmTarget.rotation = R_foreArm.rotation;
    //    //右手
    //    if (R_hand_Target && R_hand)
    //        R_hand_Target.rotation = R_hand.rotation;

    //    //右邊
    //    //右肩
    //    if (L_shoulder_Target && L_shoulder)
    //        L_shoulder_Target.rotation = L_shoulder.rotation;
    //    //右腕
    //    if (L_arm_Target && L_arm)
    //        L_arm_Target.rotation = L_arm.rotation;
    //    //右前臂
    //    if (L_foreArmTarget && L_foreArm)
    //        L_foreArmTarget.rotation = L_foreArm.rotation;
    //    //右手
    //    if (L_hand_Target && L_hand)
    //        L_hand_Target.rotation = L_hand.rotation;





    //}

    private void SetRotateForTarget()
    {


        //右邊
        //右肩
        if (R_shoulder_Target && R_shoulder)
            R_shoulder.rotation = R_shoulder_Target.rotation;
        //右腕
        //if (R_arm_Target && R_arm)
        //    R_arm.rotation = R_arm_Target.rotation;
        ////右前臂
        //if (R_foreArmTarget && R_foreArm)
        //    R_foreArm.rotation = R_foreArmTarget.rotation;
        ////右手
        //if (R_hand_Target && R_hand)
        //    R_hand.rotation = R_hand_Target.rotation;



        //左邊
        //左肩
        if (L_shoulder_Target && L_shoulder)
            L_shoulder.rotation = L_shoulder_Target.rotation;
        //
        //Quaternion.Lerp(L_shoulder.rotation, L_shoulder_ROT, L_shoulderWeight * L_Weight);

        ////左腕
        //if (L_arm_Target && L_arm)
        //    L_arm.rotation =
        //        Quaternion.Lerp(L_arm.rotation, L_arm_Target.rotation, L_armWeight * L_Weight);


        ////左前臂
        //if (L_foreArmTarget && L_foreArm)
        //    L_foreArm.rotation =
        //        Quaternion.Lerp(L_foreArm.rotation, L_foreArmTarget.rotation, L_foreArmWeight * L_Weight);

        ////左手
        //if (L_hand_Target && L_hand)
        //    L_hand.rotation =
        //        Quaternion.Lerp(L_hand.rotation, L_hand_Target.rotation, L_handWeight * L_Weight);


    }


    void Check_PlayerState()
    {

        if (m_playerState == PlayerState.idel)
        {
            m_Animator.SetBool("Ismoving", false);
            m_Animator.SetBool("doRun", false);

        }
        else {
            m_PlayerModel.rotation = Quaternion.Slerp(m_PlayerModel.rotation, Quaternion.LookRotation(bodyForward), 8 * Tdt);


            followForward = bodyForward;


        }
    

         if (m_playerState == PlayerState.Walk)
        {
            m_Animator.SetBool("Ismoving", true);
            m_Animator.SetBool("doRun", false);

        }


        else if (m_playerState == PlayerState.Run)
        {
            m_Animator.SetBool("Ismoving", true);
            m_Animator.SetBool("doRun", true);
        }
    }



    void Check_R_armState()
    {

        AnimatorStateInfo m_ani;
        m_ani = m_Animator.GetCurrentAnimatorStateInfo(2);


        if (m_Animator.GetBool("ShowArms"))
        {
  
            m_R_armState = R_ArmState.ShowArms;

        }

        else if (m_Animator.GetCurrentAnimatorStateInfo(2).IsName("Pick") && m_ani.normalizedTime < 0.9)
        {
            m_R_armState = R_ArmState.GetArms;

        }

        else if (m_Animator.GetCurrentAnimatorStateInfo(2).IsName("Throw_ready"))
        {
            m_R_armState = R_ArmState.Throw;
        }

        else if (m_Animator.GetCurrentAnimatorStateInfo(2).IsName("Throw" )&& m_ani.normalizedTime < 0.6)
        {
            m_R_armState = R_ArmState.Throw;
        }
       

        else m_R_armState = R_ArmState.Relax;


    }


    void Check_R_handState()
    {
        if (m_R_handState == R_handState.Relax)
        {
            m_Animator.SetLayerWeight(4, Mathf.Lerp(m_Animator.GetLayerWeight(4), 0, Tdt * ChangeLayerSpeed));

        }
        if (m_R_handState == R_handState.GetArms)
        {

            m_Animator.SetLayerWeight(4, Mathf.Lerp(m_Animator.GetLayerWeight(4), 1, Tdt * ChangeLayerSpeed));

        }
    }


    void Check_L_armState()
    {

       

        if (m_L_armState == L_ArmState.Relax)
        {
            m_Animator.SetLayerWeight(1, Mathf.Lerp(m_Animator.GetLayerWeight(1), 0, Tdt * ChangeLayerSpeed*0.3f));

            L_shoulder_ROT = Quaternion.Lerp(L_shoulder_ROT, L_armRelaxTarget.rotation, Tdt *30f);

            L_armRelaxWeight = Mathf.Lerp(L_armRelaxWeight,1, Tdt *2.5F);


        }

        else if (m_L_armState == L_ArmState.Flashlight)
        {

            m_Animator.SetLayerWeight(1, Mathf.Lerp(m_Animator.GetLayerWeight(1), 0.98f, Tdt * ChangeLayerSpeed));

            L_shoulder_ROT = Quaternion.Lerp(L_shoulder_ROT, L_shoulder_Target.rotation, Tdt * 45F);


            L_armRelaxWeight = Mathf.Lerp(L_armRelaxWeight, 0, Tdt * 3F);


        }

    }
  

    void Check_L_handState()
    {

        if (m_L_handState == L_handState.Relax)
        {
            m_Animator.SetLayerWeight(3, Mathf.Lerp(m_Animator.GetLayerWeight(3), 0 , Tdt * ChangeLayerSpeed));

        }

        if (m_L_handState == L_handState.GetFlashlight)
        {
            m_Animator.SetLayerWeight(3, Mathf.Lerp(m_Animator.GetLayerWeight(3), 1, Tdt * ChangeLayerSpeed));

        }
    }

    void DebugOverTurn(Vector3 body, Vector3 feet)
    {

        Reset_Turn_ani();

       


            if (Vector3.Angle(body, feet) >= 40)

            {
            Vector3 V3zero = new Vector3(0, 0, 0);
            feet = body;

            //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(bodyForward), 16 * Tdt);

        }
       

    }








    public PlayerState GetPlayerState()
    {
        return m_playerState;
    }
    public void SetPlayerState(PlayerState m_state)
    {
        m_playerState = m_state;

       
    }
    public R_handState GetR_handState()
    {
        return m_R_handState;
    }
    public void SetR_handState(R_handState m_state)
    {
        m_R_handState = m_state;

        
    }
    public L_handState GetL_handState()
    {
        return m_L_handState;
    }
    public void SetL_handState(L_handState m_state)
    {
        m_L_handState = m_state;


    }
    public R_ArmState GetR_ArmState()
    {
        return m_R_armState;
    }
    public void SetR_ArmState(R_ArmState m_state)
    {
        m_R_armState = m_state;


    }
    public L_ArmState GetL_ArmState()
    {
        return m_L_armState;
    }
    public void SetL_ArmState(L_ArmState m_state)
    {
        m_L_armState = m_state;


    }

 void checkMBR()
    {

        if (m_R_armState == R_ArmState.Throw)
            m_R_handState = R_handState.Relax;


        if (Input.GetKey(KeyCode.Mouse1) == false)
            m_R_armState = R_ArmState.Relax;

        else m_R_armState = R_ArmState.ShowArms;

    }


    void SetR_ArmAniWeight()
    {
        if (m_R_armState == R_ArmState.Relax)
        {
            m_Animator.SetLayerWeight(2, Mathf.Lerp(m_Animator.GetLayerWeight(2), 0, Tdt*8F));
        }
        else 
        {
            m_Animator.SetLayerWeight(2, Mathf.Lerp(m_Animator.GetLayerWeight(2), 0.98f, Tdt* 8F));

        }
        
        
        if (R_armRelaxWeight>0.5)R_armRelaxWeight = 0.5f;// = m_Animator.GetLayerWeight(2);
     
    }

    void SetL_ArmAniWeight()
    {
        if (m_L_armState == L_ArmState.Relax)
        {
            m_Animator.SetLayerWeight(1, Mathf.Lerp(m_Animator.GetLayerWeight(1), 0, Tdt * 12F));
        }
        else
        {
            m_Animator.SetLayerWeight(1, Mathf.Lerp(m_Animator.GetLayerWeight(1), 0.95f, Tdt * 12F));
        }

        if (L_armRelaxWeight < 0.5) R_armRelaxWeight = 0.5f;// = m_Animator.GetLayerWeight(2);
       

    }



    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //Debug.Log("Normal vector we collided at: " + hit.normal);
        GroundedCheckerRay = new Ray(this.transform.position + new Vector3(0, 0.1f, 0), -Vector3.up);

       
            hitpointN = hit.normal;

      

    }


    void slideSet()
    {
        if ((!Physics.Raycast(GroundedCheckerRay, out hitpoint, checkHight)
          || (Vector3.Angle(Vector3.up, hitpoint.normal) > 55)
          || hitpoint.distance > 0.7f)&& m_Ctrler.isGrounded)
        {

            fallMove = hitpointN;
            fallMove.y = 0;
            fallMove.Normalize();
            m_Ctrler.Move(fallMove * Tdt * fallPower);

        }

    }



        void SetGravity()
    {


        GroundedCheckerRay = new Ray(this.transform.position + new Vector3(0, 0.1f, 0), -Vector3.up);


        //Debug.Log("isGrounded" + m_Ctrler.isGrounded);

        if (m_Ctrler.isGrounded == false
            && Physics.Raycast(GroundedCheckerRay, out hitpoint, checkHight) == false)
        { m_IsGrounded = false; }
        else
        { m_IsGrounded = true; }


        m_Animator.SetBool("IsGround", m_IsGrounded);



        if (m_playerState != PlayerState.Run)
        {

           m_Ctrler.Move(new Vector3(0, -5f*Tdt, 0));
        }

       m_Ctrler.SimpleMove(new Vector3(0, 0, 0));

        

    }











}
