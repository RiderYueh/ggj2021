﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightSet : MonoBehaviour
{
    public bool openFlashing = false;

    [Range(0, 20)]
    public float vRange = 12;

    public Transform FlashLight;

    public Transform FlashLightModel;

    MonsterDistanceCheaker m_Mdc;

    Animator m_Animator;
    Light m_light;


    //  Random m_random;
    [Range(0,1)]
    public float minlight=1;

    [Range(0, 1)]

    public float maxlight = 1;


    [Range(0, 10)]

    public float time = 10;



    private float timeOut = 0;

    private float Tdt;


    private void Start()
    {
        var m_FlashLight = this.GetComponent<FlashLightSet>().FlashLight;

        m_Mdc = this.GetComponent<MonsterDistanceCheaker>();
       m_light = FlashLight.GetComponent<Light>();
        m_Animator = this.GetComponent<Player_ctrler_bata>().m_Animator;


    }

    private void Update()
    {
        Tdt = Time.deltaTime;


      //  m_Mdc.monsterMinDistance



        if (m_Animator.GetLayerWeight(1) >= 0.9f)
        {
            m_light.enabled = true;

            if (openFlashing == true)
            {



                if (m_Mdc.monsterMinDistance < vRange)
                {
                    float mathTest;
                    mathTest = m_Mdc.monsterMinDistance / vRange;


                    // time = Mathf.Lerp(0f, 50, mathTest);

                    time = Mathf.Pow(mathTest, 2);

                }
                else
                {
                    time = 999;

                }






                if (minlight > maxlight)
                    minlight = maxlight;

                if (maxlight < minlight)
                    maxlight = minlight;



                if (m_light.intensity != maxlight)
                    m_light.intensity = maxlight;


                if (timeOut >= time)
                {
                    m_light.intensity = Random.Range(minlight, maxlight);

                    timeOut = 0;
                }
                else timeOut += Tdt;

            }

        }

        else m_light.enabled =false;

    }










}
