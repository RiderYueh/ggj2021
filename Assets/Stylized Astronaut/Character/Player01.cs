﻿using UnityEngine;
using System.Collections;

public class Player01 : MonoBehaviour
{
	static public Player01 Instance;
	public Camera mainCamera;
	public GameObject handlight;
	public float speed = 5;
	public LayerMask floorMask;
	private Animator anim;


	CharacterController m_Ctrler;
	Vector3 m_movepos;
	float m_move_h;
	float m_move_v;
	Vector3 m_moveDirection;
	Vector3 m_moveDirection2;

	private void Awake()
	{
		if (Instance == null)
			Instance=this;
	}

	void Start()
	{
		m_Ctrler = this.GetComponent<CharacterController>();
		anim = gameObject.GetComponentInChildren<Animator>();
		mainCamera.transform.position =new Vector3(transform.position.x, transform.position.y + 20f, transform.position.z);
	}

	void Update()
	{

		lightturn();
		SetAnim();
		moveCharacter();
	}

	void LateUpdate()
	{
		MoveCamera();
	}

	Ray camRay;
	RaycastHit floorHit;

	void moveCharacter()
	{
		SetAnim(0);
		if (m_Ctrler.isGrounded)
		{
			m_move_h = Input.GetAxis("Horizontal");
			m_move_v = Input.GetAxis("Vertical");
			if (m_move_v != 0 || m_move_h != 0) SetAnim();

			m_moveDirection = Vector3.right * m_move_h;
			m_moveDirection2 = Vector3.forward * m_move_v;
		}

		m_Ctrler.Move((m_moveDirection + m_moveDirection2)*speed * Time.deltaTime);


		 /*m_move_h = Input.GetAxis("Horizontal");
		 m_move_v = Input.GetAxis("Vertical");
		 if (m_move_v != 0 || m_move_h != 0) SetAnim();
		 m_movepos.x = m_move_h;
		 m_movepos.z = m_move_v;
		 m_Ctrler.SimpleMove(m_movepos.normalized * speed);*/

		 // 建立一個物理射線，獲取滑鼠的座標
		 camRay = mainCamera.ScreenPointToRay(Input.mousePosition);

		// 如果射線打擊到物體，並且物體屬於floorMask層
		if (Physics.Raycast(camRay, out floorHit, 1000, floorMask))
		{
			Vector3 targetVec3 = floorHit.point;
			transform.LookAt(new Vector3(targetVec3.x, transform.position.y, targetVec3.z));
		}
	}

	void SetAnim(int state = 1)
	{
		anim.SetInteger("AnimationPar", state);
	}

	void MoveCamera()
	{
		mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position,
		new Vector3(transform.position.x, transform.position.y + 20f, transform.position.z)
		, 5f*Time.deltaTime);


		




	}

	void lightturn()
	{
		if (Input.GetKeyDown("f"))
		{
			if (handlight.activeSelf)
			{
				handlight.SetActive(false);
			}
			else
			{
				handlight.SetActive(true);
			}
			//FindObjectOfType<SoundManager>().Play("flashLightOnOff");
		}
	}


	//碰撞測試
	public GameObject targetNow;
	private void OnTriggerEnter(Collider collider)
	{
		if (collider.tag == "Item")
		{
			targetNow = collider.gameObject;
			UI_Inventory.Instance.OnOpenButton(true);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (targetNow == other.gameObject)
		{
			UI_Inventory.Instance.OnOpenButton(false);
		}
	}
}
