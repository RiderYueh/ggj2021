using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour 
{
	static public Player Instance;
	public Camera mainCamera;
	public GameObject handlight;
	public float speed = 5;
	public LayerMask floorMask;
	private Animator anim;
	private float Satiety;
	private bool OldWalkState = false;
	private bool WalkOnGarbage = false;
	private bool OldWalkOnGarbage = false;

	[SerializeField] private float defSatiety;
	[SerializeField] private float defHungerInterval;
	[SerializeField] private float defHunger;
	[SerializeField] private float defHungerSoundInterval;
	[SerializeField] private float defSatietyStep1;
	[SerializeField] private float defSatietyStep2;
	[SerializeField] private float defSatietyStep3;

	CharacterController m_Ctrler;
	Vector3 m_movepos;
	float m_move_h;
	float m_move_v;
	Vector3 m_moveDirection;
	Vector3 m_moveDirection2;
	Vector3 m_finalDirection;
	private SoundManager m_SoundManager;
	[HideInInspector] public bool isGetLight = false;
	[HideInInspector] public bool isSavePeople = false;

	private void Awake()
    {
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(transform.parent.gameObject);
		}
        else if (Instance != this)
        {
            Destroy(transform.parent.gameObject);
        }


    }

    void Start () 
	{
		OpenHandLight();
		m_Ctrler = this.GetComponent<CharacterController>();
		anim = gameObject.GetComponentInChildren<Animator>();
		m_SoundManager = FindObjectOfType<SoundManager>();
		Satiety = defSatiety;
		InvokeRepeating("dealSatiety", 1.0f, defHungerInterval);
		InvokeRepeating("dealHungerSound", 1.0f, defHungerSoundInterval);


		mainCamera.transform.position = new Vector3(transform.position.x, 14.527f, transform.position.z);

	}

	float hitWallTimer = 0;

	void Update (){


		lightturn();
		SetAnim();
		moveCharacter();

		hitWallTimer += Time.deltaTime;

		if(Input.GetKeyDown(KeyCode.R))
        {
			ReloadScene();

		}
	}

	void ReloadScene()
    {
		ChangeSceneGG(SceneManager.GetActiveScene().buildIndex);
	}

	void LateUpdate()
    {
		MoveCamera();
	}

	Ray camRay;
	RaycastHit floorHit;
	void moveCharacter()
	{
		bool OnWalk = false;
		SetAnim(0);
		if (m_Ctrler.isGrounded)
		{
			m_move_h = Input.GetAxis("Horizontal");
			m_move_v = Input.GetAxis("Vertical");

			if(isDie)
            {
				m_move_h = 0;
				m_move_v = 0;

			}

			if (m_move_v != 0 || m_move_h != 0)
            {
				SetAnim();
				OnWalk = true;
			}

			
			m_moveDirection = Vector3.right * m_move_h;
			m_moveDirection2 = Vector3.forward * m_move_v;
			m_moveDirection.y -= 20 * Time.deltaTime;
			
		}

		Vector3 gravity = new Vector3(0, Time.deltaTime * 20, 0);
		m_finalDirection = (m_moveDirection + m_moveDirection2) * speed * Time.deltaTime - gravity;

		m_Ctrler.Move(m_finalDirection);

		dealWalkSound(OnWalk, WalkOnGarbage);

		// �إߤ@�Ӫ��z�g�u�A����ƹ����y��
		camRay = mainCamera.ScreenPointToRay(Input.mousePosition);

		// �p�G�g�u�����쪫��A�åB�����ݩ�floorMask�h
		if (Physics.Raycast(camRay, out floorHit, 1000, floorMask))
		{
			Vector3 targetVec3 = floorHit.point;

			if (!isDie)
				transform.LookAt(new Vector3(targetVec3.x, transform.position.y, targetVec3.z));
		}
	}

	void SetAnim(int state = 1)
    {
		anim.SetInteger("AnimationPar", state);
	}

	Vector3 playerPos;
	void MoveCamera()
	{
		float tempHeight = 14.527f;
		if(SceneManager.GetActiveScene().buildIndex == 8)
        {
			tempHeight = 20;
		}
		playerPos = transform.position;

		mainCamera.transform.position = new Vector3(transform.position.x, tempHeight, transform.position.z);

	}

	public void OpenHandLight()
    {
		if (!isGetLight) return;
		handlight.SetActive(true);
	}
	
	void lightturn()
	{
		if (!isGetLight) return;
		if (Input.GetKeyDown("f"))
		{
			if(handlight.activeSelf)
            {
				handlight.SetActive(false);
			}
            else
            {
				handlight.SetActive(true);
			}
			m_SoundManager.Play("flashlight");
		}
	}


	//�I������
	public GameObject targetNow;
	private void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Item")
        {
			targetNow = collider.gameObject;
			UI_Inventory.Instance.OnOpenButton(true);
		}
		if (collider.tag == "walkOnGarbage")
		{
			WalkOnGarbage = true;
		}
	}

    private void OnTriggerExit(Collider other)
    {
        if(targetNow == other.gameObject)
        {
			UI_Inventory.Instance.OnOpenButton(false);
		}
		if (other.tag == "walkOnGarbage")
		{
			WalkOnGarbage = false;
		}
	}

	//撞牆聲音
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if (hitWallTimer < 0.2f) return;

		if (hit.collider.gameObject.tag == "Wall")
		{
			
			var dir = (new Vector3(hit.point.x , playerPos.y, hit.point.z) - new Vector3(playerPos.x , playerPos.y, playerPos.z)).normalized;
			float power = 0.01f;
			Vector3 dir2 = new Vector3(dir.x , 0 , dir.z);


			if (dir.x < power && Mathf.Abs(dir.x) > power && Mathf.Abs(m_move_h) > power )
            {
				//Debug.Log("左");
				m_SoundManager.PlayHitWallSound(playerPos + (dir2));
				hitWallTimer = 0;
			}
			if (dir.x > power && Mathf.Abs(dir.x) > power && Mathf.Abs(m_move_h) > power)
			{
				//Debug.Log("右");
				m_SoundManager.PlayHitWallSound(playerPos + (dir2));
				hitWallTimer = 0;
			}
			if (dir.z > power && Mathf.Abs(dir.z) > power && Mathf.Abs(m_move_v) > power)
			{
				//Debug.Log("上");
				m_SoundManager.PlayHitWallSound(playerPos + (dir2));
				hitWallTimer = 0;
			}
			if (dir.z < power && Mathf.Abs(dir.z) > power && Mathf.Abs(m_move_v) > power)
			{
				//Debug.Log("下");
				m_SoundManager.PlayHitWallSound(playerPos + (dir2));
				hitWallTimer = 0;
			}
		}
	}

	public void eating(int howSatiety)
    {
		Satiety += howSatiety;
		if (Satiety > 100) Satiety = 100;
		m_SoundManager.Play("eating");
	}

	void dealSatiety()
    {
		if(Satiety > 0)
        {
			Satiety -= 1;
		}
		
		Debug.Log(Satiety);
		if (Satiety <= 0)
		{
			//trigger game over
			Debug.Log("Too Hungry");
			m_SoundManager.Stop("hungry2");
			m_SoundManager.Stop("hungry3");
			m_SoundManager.Play("hungry4");
			CancelInvoke("dealSatiety");
			CancelInvoke("dealHungerSound");
			StartCoroutine(DieTime());
			isDie = true;
		}
	}
	bool isDie;
	IEnumerator DieTime()
    {
		yield return new WaitForSeconds(3);
		ReloadScene();
	}

	int hungrySound = 0;
	void dealHungerSound()
    {
		hungrySound += 1;
		if (hungrySound < 10) return;
		if (Satiety < defSatietyStep3)
		{
			m_SoundManager.Stop("hungry2");
			m_SoundManager.Play("hungry3");
			hungrySound = 0;
		}
		else if (Satiety < defSatietyStep2)
		{
			m_SoundManager.Stop("hungry1");
			m_SoundManager.Play("hungry2");
			hungrySound = 0;
		}
		else if (Satiety < defSatietyStep1)
		{
			m_SoundManager.Play("hungry1");
			hungrySound = 0;
		}
	}

	void dealWalkSound(bool onWalk, bool walkOnGarbage)
    {
		if(OldWalkState != onWalk || OldWalkOnGarbage != walkOnGarbage)
        {
			OldWalkState = onWalk;
			OldWalkOnGarbage = walkOnGarbage;
			if (OldWalkState)
            {
				if(OldWalkOnGarbage)
                {
					m_SoundManager.Play("walkOnGarbage");
					m_SoundManager.Stop("walk");
				}
                else
                {
					m_SoundManager.Play("walk");
					m_SoundManager.Stop("walkOnGarbage");
				}
			}
            else
            {
				m_SoundManager.Stop("walk");
				m_SoundManager.Stop("walkOnGarbage");
			}
		}
    }

	public void ChangeSceneGG(int gotoID)
	{
		Debug.Log("GO");
		Satiety = 100;
		isDie = false;
		CancelInvoke("dealSatiety");
		CancelInvoke("dealHungerSound");
		InvokeRepeating("dealSatiety", 1.0f, defHungerInterval);
		InvokeRepeating("dealHungerSound", 1.0f, defHungerSoundInterval);
		Player.Instance.GetComponent<CharacterController>().enabled = false;

		if (gotoID == 0)
		{

			Player.Instance.transform.parent.position = new Vector3(-2.2f, 2.38f, 13.3f);
			Player.Instance.transform.localPosition = new Vector3(0, 0, 0);
		}
		if (gotoID == 1)
		{

			Player.Instance.transform.parent.position = new Vector3(-16.39f, 5, -8.35f);
			Player.Instance.transform.localPosition = new Vector3(0, 0, 0);
		}
		if (gotoID >= 2 && gotoID <= 7)
		{
			Player.Instance.transform.parent.position = new Vector3(0, 0, 0);
			Player.Instance.transform.localPosition = new Vector3(0, 0, 0);
			gotoID = 2;
		}
		if (gotoID == 8)
		{
			Player.Instance.transform.parent.position = new Vector3(2.52f, 1.041504f, -15.99f);
			Player.Instance.transform.localPosition = new Vector3(0, 0, 0);
		}

		if (gotoID == 9)
		{
			if(Player.Instance.isSavePeople)
            {
				gotoID = 10;

			}
			Player.Instance.transform.parent.gameObject.SetActive(false);
		}

		SceneManager.LoadScene(gotoID);
		Player.Instance.GetComponent<CharacterController>().enabled = true;
	}
}
