﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumer : MonoBehaviour
{

    public bool IsEating=true;

    [Range(0,4)]
    public int m_skinID = 0;


    GameObject[] portions;
    int currentIndex;
    float lastChange;
    float interval = 1f;

    void Start()
    {
        bool skipFirst = transform.childCount > 4;
        portions = new GameObject[skipFirst ? transform.childCount-1 : transform.childCount];
        for (int i = 0; i < portions.Length; i++)
        {
            portions[i] = transform.GetChild(skipFirst ? i + 1 : i).gameObject;
            if (portions[i].activeInHierarchy)
                currentIndex = i;
        }


    }

    void Update()
    {


        if (IsEating == true)
        {
            if (Time.time - lastChange > interval)
            {
                Consume();
                lastChange = Time.time;
            }
        }

        else SetSkin(m_skinID);





    }

    void Consume()
    {
        if (currentIndex != portions.Length)
            portions[currentIndex].SetActive(false);
        currentIndex++;
        if (currentIndex > portions.Length)
            currentIndex = 0;
        else if (currentIndex == portions.Length)
            return;
        portions[currentIndex].SetActive(true);
    }


    void SetSkin(int skinID)
    {


        for(int i=0; i< portions.Length;i++)
        {

            portions[i].SetActive(false);
        }

        if (skinID >= 0 && skinID < portions.Length)
            portions[skinID].SetActive(true);


        else return;

    }







}
