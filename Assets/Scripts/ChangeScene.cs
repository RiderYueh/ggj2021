﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public int gotoID = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.N) && Input.GetKey(KeyCode.M))
        {
            Player.Instance.ChangeSceneGG(gotoID);
        }

        if(Vector3.Distance(transform.position , Player.Instance.transform.position) < 2)
        {
            Player.Instance.ChangeSceneGG(gotoID);
        }
    }

    
}
