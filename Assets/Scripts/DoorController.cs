﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{
    public int id;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.onDoorwayTrigerEnter += OnDoorwayOpen;

    }

    private void OnDoorwayOpen(int id)
    {
        if (id == this.id)
        {
            if (id == 0)
            {
                Debug.Log("Left Door Open, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(5.9f, 0f, 0f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
            }
            else if (id == 1)
            {
                Debug.Log("Up Door Open, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(0f, 0f, -5.8f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;    
            }
            else if (id == 2) //用不到
            {
                Debug.Log("Right Door Open, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(-5.9f, 0f, 0f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
            }
            else if (id == 3)
            {
                Debug.Log("Down Door Open, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(0f, 0f, 5.8f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
            }
            else if (id == 8)
            {
                Debug.Log("right door is right! L1 to L2");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(-5.8f, 0f, 0f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                FindObjectOfType<SoundManager>().Play("wrong");
                SceneManager.LoadScene(3);
                
            }
            else if (id == 4)
            {
                Debug.Log("Left Door is wrong, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                SceneManager.LoadScene(2);
            }
            else if (id == 5)
            {
                Debug.Log("Up Door is wrong, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                SceneManager.LoadScene(2);
            }
            else if (id == 6)
            {
                Debug.Log("Right Door is wrong, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(-5.8f, 0f, 0f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                SceneManager.LoadScene(2);
            }
            else if (id == 7)
            {
                Debug.Log("Down Door is wrong, ID : " + id);
                FindObjectOfType<SoundManager>().Play("wrong");
                SceneManager.LoadScene(2);
            }
            else if (id == 9)
            {
                Debug.Log("down door is right! L2 to L3");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(0f, 0f, 5.8f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                SceneManager.LoadScene(4);
            }
            else if (id == 10)
            {
                Debug.Log("up door is right! L3 to L4");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(0f, 0f, -5.8f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                SceneManager.LoadScene(5);
            }
            else if (id == 11)
            {
                Debug.Log("down door is right! L4 to L5");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(0f, 0f, 5.8f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                SceneManager.LoadScene(6);
            }
            else if (id == 12)
            {
                Debug.Log("left door is right! L5 to top");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(5.9f, 0f, 0f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                SceneManager.LoadScene(7);
            }
            //else if (id == 50)
            //{
            //    Debug.Log("發電機開啟");
            //    if(UI_Inventory.Instance.list_PlayerItem.Count == 3)
            //    {
            //        FindObjectOfType<SoundManager>().Play("success");
            //    }
            //}
            else if (id == 66)
            {
                Debug.Log("傳送至塔");
                Player.Instance.GetComponent<CharacterController>().enabled = false;
                Player.Instance.transform.position = new Vector3(-5.9f, 0f, 0f);
                Player.Instance.GetComponent<CharacterController>().enabled = true;
                SceneManager.LoadScene(2);
                Debug.Log("傳送至塔了沒");
            }
        }
    }
}
