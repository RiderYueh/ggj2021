﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWall : MonoBehaviour
{
    public Vector3 targetPos;
    public float speed = 3;
    public int id;

    private Vector3 oriPos;
    private AudioSource m_AudioSource;
    void Start()
    {
        oriPos = transform.localPosition;
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    bool isGoTarget = false;
    bool isHit = false;
    bool isPlaySound = false;
    Vector3 nor;
    void Update()
    {
        Vector3 triggerPos = new Vector3(0,0,0);
        if (isGoTarget)
        {
            nor = (targetPos - transform.localPosition).normalized;
            triggerPos = targetPos;
        }
        else
        {
            nor = (oriPos - transform.localPosition).normalized;
            triggerPos = oriPos;
        }

        RaycastHit hit;
        Debug.DrawRay(transform.position + new Vector3(0, 1, 0), nor * 2, Color.yellow);

        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), nor, out hit, 2))
        {
            isHit = true;
        }
        else
        {
            isHit = false;
        }

        if (isHit) return;


        if (Vector3.Distance(triggerPos, transform.localPosition) > 0.1f)
        {
            transform.localPosition += nor * Time.deltaTime * speed;
            if(!isPlaySound)
            {
                m_AudioSource.Play();
                isPlaySound = true;
            }
        }
        else
        {
            m_AudioSource.Stop();
            isPlaySound = false;
        }
        
    }

    public void OnTrigger()
    {
        isGoTarget = !isGoTarget;
        m_AudioSource.Stop();
        isPlaySound = false;
    }

}
