﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDoorManager : MonoBehaviour
{
    public List<MoveWall> list_moveWall;
    public GameObject door;
    public void MoveDoor(int id)
    {
        list_moveWall[id - 1].OnTrigger();
    }

    public void DoorTrigger()
    {
        gameObject.SetActive(false);
        door.SetActive(false);
    }
}
