﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Inventory : MonoBehaviour
{
    static public UI_Inventory Instance;
    public Transform itemSlotContainer;
    public Transform itemSlotTemplate;
    public GameObject goButton;

    public List<Item> list_PlayerItem;

    public GameObject finalDeadPeople;

    [SerializeField] private float defItemSlotCellSize;
    [SerializeField] private GameObject Dlight;
    private MoveDoorManager moveDoorManager;
    private SoundManager soundManager;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        list_PlayerItem = new List<Item>();
        moveDoorManager = FindObjectOfType<MoveDoorManager>();
        soundManager = FindObjectOfType<SoundManager>();
        Refresh();
    }

    void Refresh()
    {
        foreach(Transform child in itemSlotContainer)
        {
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }

        int x = 0;
        int y = 0;
        float itemSlotCellSize = defItemSlotCellSize;
        foreach (Item item in list_PlayerItem)
        {
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);
            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, y * itemSlotCellSize);
            Image image = itemSlotRectTransform.Find("Image").GetComponent<Image>();
            if(item.GetSprite() != null)
            {
                image.sprite = item.GetSprite();
            }
            ++x;
            if(x > 4)
            {
                x = 0;
                ++y;
            }
        }

        if(list_PlayerItem.Count == 0)
        {
            GameObject childBackGround = Instance.transform.GetChild(0).gameObject;
            childBackGround.SetActive(false);
        }
        else
        {
            GameObject childBackGround = Instance.transform.GetChild(0).gameObject;
            childBackGround.SetActive(true);
        }
    }

    public void ConsumeItem()
    {
        list_PlayerItem.Clear();
        Refresh();
        soundManager.Play("pickup");
    }

    //================UI Event================
    public void OnClickGetItem()
    {
        if(Player.Instance.targetNow != null)
        {
            if(Player.Instance.targetNow.GetComponent<Item>().itemType == Item.ItemType.Carrion)
            {
                Player.Instance.eating(Player.Instance.targetNow.GetComponent<Item>().Satiety);
                Destroy(Player.Instance.targetNow);
            }
            else if (Player.Instance.targetNow.GetComponent<Item>().itemType == Item.ItemType.Light)
            {
                soundManager.Play("pickup");
                Player.Instance.isGetLight = true;
                Player.Instance.OpenHandLight();
                Destroy(Player.Instance.targetNow);
            }
            else if (Player.Instance.targetNow.GetComponent<Item>().itemType == Item.ItemType.PowerGeneration)
            {
                if(list_PlayerItem.Count >= 3)
                {
                    ConsumeItem();                  
                    Player.Instance.targetNow.tag = "Untagged";
                    if (Dlight != null)
                    {
                        Dlight.SetActive(true);
                    }
                    Player.Instance.targetNow.GetComponent<AudioSource>().Play();
                }
                else
                {
                    soundManager.Play("wrong");
                }
            }
            else if (Player.Instance.targetNow.GetComponent<Item>().itemType == Item.ItemType.MoveWallTrigger)
            {
                moveDoorManager.MoveDoor(Player.Instance.targetNow.GetComponent<Item>().id);
                soundManager.Play("flashlight");
            }

            else if (Player.Instance.targetNow.GetComponent<Item>().itemType == Item.ItemType.DoorTrigger)
            {
                moveDoorManager.DoorTrigger();
                soundManager.Play("flashlight");
            }

            else if (Player.Instance.targetNow.GetComponent<Item>().itemType == Item.ItemType.People)
            {
                Destroy(finalDeadPeople);
                soundManager.Play("save");
                Player.Instance.isSavePeople = true;
            }

            else
            {
                list_PlayerItem.Add(Player.Instance.targetNow.GetComponent<Item>());
                soundManager.Play("pickup");
                Destroy(Player.Instance.targetNow);
            }
            Refresh();
            OnOpenButton(false);
        }
        
    }

    public void OnOpenButton(bool isOpen = true)
    {
        if (Player.Instance.targetNow.GetComponent<Item>().itemOperation == Item.ItemOperation.Eat)
        {
            SetButtonText("Eat");
        }
        else if(Player.Instance.targetNow.GetComponent<Item>().itemOperation == Item.ItemOperation.Take)
        {
            SetButtonText("Take");
        }
        else if (Player.Instance.targetNow.GetComponent<Item>().itemOperation == Item.ItemOperation.Trigger)
        {
            SetButtonText("Trigger");
        }
        else if (Player.Instance.targetNow.GetComponent<Item>().itemOperation == Item.ItemOperation.People)
        {
            SetButtonText("Save");
        }
        else
        {
            SetButtonText(" ");
        }
        goButton.SetActive(isOpen);
    }

    void SetButtonText(string txt)
    {
        GameObject childText = goButton.transform.GetChild(0).gameObject;
        Text text = childText.GetComponent<Text>();
        text.text = txt;
    }

}
