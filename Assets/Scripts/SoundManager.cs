﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public Camera fpsCam;
    public Sound[] sounds;
    internal SoundManager sManager;
    
    public Slider Volume;

    public GameObject hitWallSound;
    
    // Awake() is called right before it
    void Awake()
    {
       foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.Loop;
        }        
    }
       
    
    void Start()
    {   
        //Play("arena");
    }

    //void Update()
    //{
    //    Volume.value = sounds[0].volume;
    //}

    public void Play (string title)
    {
        //using System to use Array
        //look into sounds for sound which its "title" is "tile"
        Sound s = Array.Find(sounds, sound => sound.title == title);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + title + "not found.");
            return;
        }
        if (s.source == null) return;
        s.source.volume = 1;
        s.source.Play(); 
    }
    public void Stop (string title)
    {
        Sound s = Array.Find(sounds, sound => sound.title == title);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + title + "not found.");
            return;
        }
        if (s.source == null) return;
        s.source.volume = 0;
        s.source.Stop();
    }
    public void playVolume()
    {
        foreach (Sound s in sounds)
        {
            s.source.volume = s.volume*Volume.value;
        }
    }

    public void PlayHitWallSound(Vector3 pos)
    {
        if(hitWallSound != null)
        {
            GameObject go = Instantiate(hitWallSound, pos , hitWallSound.transform.rotation) as GameObject;
            Destroy(go , 1);
        }
    }


    //FindObjectOfType<SoundManager>().Play("檔案名稱");
    //FindObjectOfType<SoundManager>().Play(Gun.weapon2);
}

