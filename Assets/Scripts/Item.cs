﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public enum ItemType
    {
        Carrion,
        Key1,
        Key2,
        Key3,
        Trigger,
        Light,
        PowerGeneration,
        MoveWallTrigger,
        DoorTrigger,
        People,
    }

    public enum ItemOperation
    {
        Eat,
        Take,
        Trigger,
        People,
    }

    public ItemType itemType;
    public ItemOperation itemOperation;
    public int amount;
    public int Satiety;
    public int id;

    public Sprite GetSprite()
    {
        switch(itemType)
        {
            default:
            case ItemType.Key1: return ItemAssets.Instance.Key1Sprite;
            case ItemType.Key2: return ItemAssets.Instance.Key2Sprite;
            case ItemType.Key3: return ItemAssets.Instance.Key3Sprite;
            case ItemType.Trigger: return ItemAssets.Instance.TriggerSprite;
        }
    }
}
