﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public static Move m_Instance;
    public Camera mainCamera;
    public GameObject handlight;
    public float speed = 5;
    // Start is called before the first frame update
    public LayerMask floorMask;
    CharacterController m_Ctrler;
    Vector3 m_movepos;
    float m_move_h;
    float m_move_v;


    void Start()
    {
        m_Instance = this;
        m_Ctrler = this.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    [System.Obsolete]
    void Update()
    {
        MoveCamera();
        moveCharacter();
        lightturn();
        Turning();

    }

    void MoveCamera()
    {
        mainCamera.transform.position = new Vector3(transform.position.x , 14.527f, transform.position.z);
    }

    void moveCharacter()
    {

        m_move_h = Input.GetAxis("Horizontal");
        m_move_v = Input.GetAxis("Vertical");
        m_movepos.x = m_move_h;
        m_movepos.z = m_move_v;
        
        m_Ctrler.SimpleMove(m_movepos.normalized * speed);






        //if (Input.GetKey("up") | Input.GetKey("w")) 
        //{
        //    transform.position += new Vector3(0 , 0 , Time.deltaTime * speed);
        //}
        //// 按住 上鍵 時，物件每個 frame 朝自身 z 軸方向移動 0.1 公尺

        //if (Input.GetKey("down") | Input.GetKey("s"))
        //{
        //    transform.position -= new Vector3(0, 0, Time.deltaTime * speed);
        //}
        //// 按住 下鍵 時，物件每個 frame 朝自身 z 軸方向移動 -0.1 公尺

        //if (Input.GetKey("left") | Input.GetKey("a"))
        //{
        //    transform.position -= new Vector3(Time.deltaTime * speed, 0, 0);
        //}
        //// 按住 上鍵 時，物件每個 frame 朝自身 z 軸方向移動 0.1 公尺

        //if (Input.GetKey("right") | Input.GetKey("d"))
        //{
        //    transform.position += new Vector3(Time.deltaTime * speed, 0, 0);
        //}
        //// 按住 下鍵 時，物件每個 frame 朝自身 z 軸方向移動 -0.1 公尺

        ////if (Input.GetKey("q")) { transform.Rotate(0, -0.1f, 0); }
        ////// 按住 左鍵 時，物件每個 frame 以自身 y 軸為軸心旋轉 -3 度
        ////if (Input.GetKey("e")) { transform.Rotate(0, 0.1f, 0); }
        ////// 按住 右鍵 時，物件每個 frame 以自身 y 軸為軸心旋轉 3 度
    }

    [System.Obsolete]
    void lightturn()
    {
        if (handlight.active == false)
        {
            if (Input.GetKeyDown("f"))
            {
                handlight.SetActive(true);
                Debug.Log("HL");
            }

        }
        if (handlight.active == true)
        {
            if (Input.GetKeyDown("g")) { handlight.SetActive(false); }
        }

    }

    Ray camRay;
    RaycastHit floorHit;
    
    void Turning()
    {
        // 建立一個物理射線，獲取滑鼠的座標
       camRay = mainCamera.ScreenPointToRay(Input.mousePosition);

        // 如果射線打擊到物體，並且物體屬於floorMask層
        if (Physics.Raycast(camRay, out floorHit, 1000, floorMask))
        {
            Vector3 targetVec3 = floorHit.point;
            transform.LookAt(new Vector3(targetVec3.x, transform.position.y, targetVec3.z));
        }
    }

    
    
}


